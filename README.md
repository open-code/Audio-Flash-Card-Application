A flash card application that let's user record the reminders in the form of Audio 
with a Description, Category, Tag and Importance Rating for it. 

The application was built on native android platform using SQLite Database for data 
storage. MediaPlayer and AudioRecorder libraries were used to record and play media files.

Other features include:
1. A MediaPlayer to play list of Memos sequentially.
2. Option to select the amount of representations required for Player
3. Search items by Imporatance, Description or Tag.