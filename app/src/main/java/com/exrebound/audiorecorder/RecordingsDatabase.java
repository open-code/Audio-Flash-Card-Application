package com.exrebound.audiorecorder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.lang.reflect.Array;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by hp sleek book on 1/13/2015.
 */
public class RecordingsDatabase{

    public static final String KEY_ROWID = "_id";
    public static final String KEY_DESCRIPTION = "audio_description";
    public static final String KEY_CATEGORY  = "audio_category";
    public static final String KEY_TAG = "audio_tag";
    public static final String KEY_AUDIO = "audio";
    public static final String KEY_STARS = "stars";

    private static final String DATABASE_NAME = "RECORDINGS_DATABASE";
    public static final String TABLE_NAME = "RECORDINGS_TABLE";
    private static final int DATABASE_VERSION = 1;

    private DatabaseHelper mDBHelper;
    private SQLiteDatabase mDatabase;
    private Context context;

    private String[] columns = new String[]{KEY_ROWID, KEY_DESCRIPTION, KEY_CATEGORY, KEY_TAG, KEY_AUDIO,KEY_STARS};


    private class DatabaseHelper extends SQLiteOpenHelper{

    public DatabaseHelper(Context c){
        super(c, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE " + TABLE_NAME + " (" +
                        KEY_ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        KEY_DESCRIPTION + " TEXT NOT NULL, " +
                        KEY_CATEGORY + " TEXT NOT NULL, " +
                        KEY_TAG + " TEXT, " +
                        KEY_AUDIO + " BLOB, " +
                        KEY_STARS + " INTEGER);"
        );
    }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
            onCreate(db);
        }
    }


    public RecordingsDatabase(Context c){
        this.context = c;
    }

    public RecordingsDatabase open()throws SQLException{
        mDBHelper = new DatabaseHelper(this.context);
        mDatabase = mDBHelper.getWritableDatabase();
        return this;
    }

    public void close(){
        if (mDBHelper != null){
            mDBHelper.close();
        }
    }


    public long insertEntry(String Description, String Category, String Tag, byte[] Audio, int Stars){
        ContentValues cv = new ContentValues();
        cv.put(KEY_DESCRIPTION,Description);
        cv.put(KEY_CATEGORY,Category);
        cv.put(KEY_TAG,Tag);
        cv.put(KEY_AUDIO,Audio);
        cv.put(KEY_STARS,Stars);
        return mDatabase.insert(TABLE_NAME, null, cv);
    }

    public boolean deleteAll(String category){
        return mDatabase.delete(TABLE_NAME,KEY_CATEGORY +" = '" + category + "'", null) > 0;
    }


    public boolean deleteAll(){
        return mDatabase.delete(TABLE_NAME,null,null) > 0;
    }

    public ArrayList<String> getDescriptions(String category) {
        Cursor c = mDatabase.query(TABLE_NAME, columns, KEY_CATEGORY + "=?", new String[]{category}, null, null, null);
        c.moveToFirst();
        ArrayList<String> descriptions = new ArrayList<>();

            for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
                String description = c.getString(c.getColumnIndex(KEY_DESCRIPTION));
                descriptions.add(description);
                Log.d("RecordingsDatabase",description+"\n");
            }
            c.requery();
            return descriptions;
    }

    public ArrayList<String> getCategories() {
        Cursor c = mDatabase.rawQuery("SELECT DISTINCT " + KEY_CATEGORY + " FROM " + TABLE_NAME, null);
        c.moveToFirst();
        ArrayList<String> categories = new ArrayList<>();

        int iCategory = c.getColumnIndex(KEY_CATEGORY);

            for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
                String category = c.getString(iCategory);
                categories.add(category);
                Log.d("RecordingsDatabase",category+"\n");
            }
            c.requery();
            return categories;
    }

    public void deleteEntry(String description) {
        mDatabase.delete(TABLE_NAME, KEY_DESCRIPTION + "='" + description+"'", null);
    }

    //Search according to importance
    public ArrayList<String> searchByStars(int Stars){
        //Cursor c = mDatabase.query(TABLE_NAME,columns,KEY_STARS+"=?",new String[]{Stars},null,null,null);
        Cursor c = mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_STARS + " = " + Stars,null);
        if(c != null){
            c.moveToFirst();
        }
        ArrayList<String> descriptions = new ArrayList<>();

        int iDescription = c.getColumnIndex(KEY_DESCRIPTION);

        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            descriptions.add(c.getString(iDescription));
        }
        c.requery();
        return descriptions;
    }

    //Search by item tag name
    public ArrayList<String> searchByTagName(String tagName) throws SQLException{
        Cursor c = null;
            if(tagName == null || tagName.length() == 0){
                c = mDatabase.query(TABLE_NAME, columns, null, null, null, null, null);
            }else{
                c = mDatabase.query(TABLE_NAME,columns,KEY_TAG+"=? OR " + KEY_DESCRIPTION +"=? ",new String[]{tagName,tagName},null,null,null);
                //c = mDatabase.rawQuery("SELECT ")
                if(c != null){
                    c.moveToFirst();
                }
                ArrayList<String> descriptions = new ArrayList<>();

                int iDescription = c.getColumnIndex(KEY_DESCRIPTION);

                for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
                    descriptions.add(c.getString(iDescription));
                }
                c.requery();
                return descriptions;
            }
            return null;
        }

    //Get Stars associated with particular Description
    public int getStars(String description) {
        Cursor c = mDatabase.query(TABLE_NAME, new String[]{KEY_STARS}, KEY_DESCRIPTION + "=?", new String[]{description}, null, null, null);

        if(c.moveToFirst()) {
            int iStars = c.getColumnIndex(KEY_STARS);
            int stars = c.getInt(iStars);
            return stars;
        }
        return 0;
    }

    //Get raw audio file associated with particular Description
    public byte[] getAudioFile(String description) {

        Log.d("RecordingsDatabase", description + "Being Searched!");
        Cursor c = null;
        if(description != null || description.length() > 0) {
            c = mDatabase.query(TABLE_NAME, columns, KEY_DESCRIPTION + "=?", new String[]{description}, null, null, null);
            if (c.moveToFirst()) {
                if (c.getBlob(c.getColumnIndex(KEY_AUDIO)) != null && c.getBlob(c.getColumnIndex(KEY_AUDIO)).length > 0) {
                    Log.d("RecordingsDatabase", "Byte Array Found!");
                    return c.getBlob(c.getColumnIndex(KEY_AUDIO));
                } else {
                    Log.d("RecordingsDatabase", "Byte Array Not Found!");
                    return null;
                }
            }
        }
        return null;
    }

    //Get Tag associated with particular Description
    public String getTag(String description) {
        Cursor c = mDatabase.query(TABLE_NAME, new String[]{KEY_TAG}, KEY_DESCRIPTION + "=?", new String[]{description}, null, null, null);

        if(c.moveToFirst()) {
            int iTag = c.getColumnIndex(KEY_TAG);
            String tag = c.getString(iTag);
            return tag;
        }
        return null;
    }
}