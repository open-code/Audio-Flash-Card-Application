package com.exrebound.audiorecorder;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Zuhaib on 8/21/2016.
 */

//Adapter class to represent customized List Items
public class RecordingsAdapter extends ArrayAdapter<String> {

    private Context context;
    private RecordingsDatabase mDatabase;

    public RecordingsAdapter(Context c, RecordingsDatabase mDatabase, ArrayList<String> description) {
        super(c, 0, description);
        this.context = c;
        this.mDatabase = mDatabase;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        ViewHolder holder;

        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = LayoutInflater.from(context).inflate(R.layout.recordings_list_item, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        String description = getItem(position);

        holder.descriptionTextView.setTextSize(20);
        holder.descriptionTextView.setText(description);

        holder.tagTextView.setText(mDatabase.getTag(description));

        LayerDrawable d = (LayerDrawable) holder.stars.getProgressDrawable();
        d.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        holder.stars.setStepSize(1.0f);
        holder.stars.setProgress(mDatabase.getStars(description));
        //stars.setEnabled(false);

        return view;
    }

    static class ViewHolder {
        @BindView(R.id.descriptionTextView)
        TextView descriptionTextView;
        @BindView(R.id.tagTextView)
        TextView tagTextView;
        @BindView(R.id.recordingsListStars)
        RatingBar stars;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}