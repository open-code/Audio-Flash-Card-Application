package com.exrebound.audiorecorder;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.SQLException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;


public class RecordingsListActivity extends ActionBarActivity implements View.OnClickListener {

    //Identifier Tag to pass along with intent
    public static final String EXTRA_DESCRIPTION = ".Description";

    //Byte array to hold Audio
    byte[] outputFile;
    //Category string
    String category = "";

    //Database, ArrayList of descriptions and Adapter instances
    RecordingsDatabase mDatabase;
    ArrayList<String> descriptions;
    RecordingsAdapter adapter;


    //Mediaplayer instance and ArrayList to hold Audio objects
    MediaPlayer m;
    ArrayList<Audio> tracks;
    //Current position and repeatation counter
    int pos = 0;
    int repeatCounter = 1;

    //References variables
    @BindView(R.id.recordingsListView)
    ListView descriptionsList;
    @BindView(R.id.titleTextView)
    TextView titleTextView;
    @BindView(R.id.PrevButton)
    ImageButton prevButton;
    @BindView(R.id.PlayButton)
    ImageButton playButton;
    @BindView(R.id.NextButton)
    ImageButton nextButton;
    @BindView(R.id.addButton)
    ImageButton addButton;
    @BindView(R.id.counterTextView)
    TextView counterTextView;
    @BindView(R.id.subtractButton)
    ImageButton subtractButton;
    @BindView(R.id.Title)
    TextView Title;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.container)
    LinearLayout container;

    //Flag variable to indicate Paused state
    private boolean isPaused = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recordings_list);
        ButterKnife.bind(this);

        Title.setText("Memos");
        setSupportActionBar(toolbar);

        //Fetcj Category name from intent and set it as Activity title
        category = getIntent().getStringExtra(CategoryListActivity.EXTRA_CATEGORY);
        setTitle(category);

        //Instantiate Database class
        mDatabase = new RecordingsDatabase(this);
        try {
            //Open Database, setListView and set player
            mDatabase.open();
            setListView();
            setController();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    //Set up ListView
    private void setListView() {
        //Fetch descriptions from Database
        descriptions = mDatabase.getDescriptions(category);
        //Instantiate adapter and set adapter on ListView
        adapter = new RecordingsAdapter(this, mDatabase, descriptions);
        descriptionsList.setAdapter(adapter);
        //Register to be able to respond to context menu event
        registerForContextMenu(descriptionsList);
    }

    @OnItemClick(R.id.recordingsListView)
    protected void onDescriptionListItemClick(int position) {
        //Fetch item description at current position and fetch output file associated with it
        String description = (String) descriptionsList.getItemAtPosition(position);
        outputFile = mDatabase.getAudioFile(description);
        try {

            if (outputFile != null) {
                //Set description in the Player's title field and Play audio
                titleTextView.setText(description);
                play(outputFile, false);
            } else if (outputFile == null) {
                //Indicate about no audio
                titleTextView.setText("No Audio with " + description);
            } else if (m != null && m.isPlaying()) {
                //If an audio is already playing, onStopButtonClick it.
                stop();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Stop playing and clean up resources
    private void stop() {
        if(m != null) {
            m.stop();
            m.release();
            m = null;
        }
    }


    //Play method accepts and Audio File and a boolean variable about weather to play one audio after another or just once.
    public void play(byte[] outputFile, final boolean requestCode) throws Exception {

        //If no audio file
        if (outputFile == null) {
            Snackbar.make(container, "No Audio Found.", Snackbar.LENGTH_SHORT).show();
            nextTrack();
            //If current position is the last item of ArrayList
            if (pos == tracks.size() - 1) {
                //Then reinitialize the counter to (First-1) index
                pos = -1;
                //Also deduct one from Repeat counter
                repeatCounter--;
                counterTextView.setText(Integer.toString(repeatCounter));
            }
            //Else if current position is not last in the list and repeat counter is also greater than 0
            if (repeatCounter > 0) {
                //Then play next
                nextTrack();
            }
        } else {
            //Else if output file is not null
            //Set up Streams and File
            File temp = File.createTempFile("temp", "mp3", getCacheDir());
            temp.deleteOnExit();
            FileOutputStream fos = new FileOutputStream(temp);
            fos.write(outputFile);
            fos.close();

            //Stop if already playing
            if (m != null && m.isPlaying()) {
                stop();
            }

            //Otherwise instantiate MediaPlayer
            m = new MediaPlayer();
            //Instantiate FileInputStream to read from File
            FileInputStream fis = new FileInputStream(temp);
            m.setDataSource(fis.getFD());
            m.prepare();
            //Now if Repeat Flag is True
            if (requestCode) {
                //Set on completion listener and whenever an item completes playing,
                // do the previous checks and play next song
                m.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        if (pos == tracks.size() - 1) {
                            pos = -1;
                            repeatCounter--;
                            counterTextView.setText(String.valueOf(repeatCounter));
                        }

                        if (repeatCounter > 0) {
                            nextTrack();
                        }
                    }
                });
            }
            if (repeatCounter > 0) {
                m.start();
            }
        }
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.descriptions_list_menu, menu);

        MenuItem item = menu.add("Search");
        item.setIcon(android.R.drawable.ic_menu_search);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

        //Create and setup Search View
        SearchView searchView = new SearchView(RecordingsListActivity.this);
        searchView.setQueryHint("Search by Description or Tag");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                try {
                    descriptions.clear();
                    descriptions.addAll(mDatabase.searchByTagName(query));
                    adapter.notifyDataSetChanged();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                descriptions.clear();
                descriptions.addAll(mDatabase.getDescriptions(category));
                adapter.notifyDataSetChanged();
                return true;
            }
        });
        item.setActionView(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.MIStars) {

            // Strings to Show In Dialog with Radio Buttons
            final CharSequence[] items = {"One", " Two ", " Three ", " Four ", " Five "};

            // Creating and Building the Dialog
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Search By Stars");
            final AlertDialog levelDialog = builder.create();
            builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {

                    //Call searchByStars method to fetch items according to the number of Stars associated
                    switch (item) {
                        case 0:
                            searchByStars(1);
                            setTitle("1 Star Items");
                            break;
                        case 1:
                            searchByStars(2);
                            setTitle("2 Star Items");
                            break;
                        case 2:
                            searchByStars(3);
                            setTitle("3 Star Items");
                            break;
                        case 3:
                            searchByStars(4);
                            setTitle("4 Star Items");
                            break;
                        case 4:
                            searchByStars(5);
                            setTitle("5 Star Items");
                            break;
                    }
                    dialog.dismiss();

                }
            }).show();
        } else if (id == android.R.id.home) {
            //Up button setup
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Method to search items based on the number of stars associated with them
    private void searchByStars(int s) {
        //First clean up the list
        descriptions.clear();
        //This time fetch form database according to given number of stars
        descriptions.addAll(mDatabase.searchByStars(s));
        //Clear and Refresh the Local List which is holding Audio objects
        tracks.clear();
        for (String st : descriptions) {
            tracks.add(new Audio(st, mDatabase.getAudioFile(st)));
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        refresh();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(
                R.menu.descriptions_list_context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // Extract information about selected item
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int position = info.position;
        String description = descriptions.get(position).toString();
        //Perform operation based on the option selected
        switch (item.getItemId()) {
            case R.id.MIDeleteDescription:
                mDatabase.deleteEntry(description);
                refresh();
                return true;
            case R.id.MIDeleteAll:
                mDatabase.deleteAll(category);
                refresh();
                return true;
            case R.id.MIEditDescription:
                Intent i = new Intent(RecordingsListActivity.this, NewRecordingActivity.class);
                i.putExtra(CategoryListActivity.EXTRA_CATEGORY, category);
                i.putExtra(EXTRA_DESCRIPTION, description);
                //i.putExtra(EXTRA_FILENAME,filename);
                startActivityForResult(i, 0);
                return true;
        }

        return super.onContextItemSelected(item);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        refresh();
    }

    //Old refresh method with same functionality that is also present in CategoriesListActivity
    private void refresh() {
        descriptions.clear();
        descriptions.addAll(mDatabase.getDescriptions(category));
        tracks.clear();
        for (String s : descriptions) {
            tracks.add(new Audio(s, mDatabase.getAudioFile(s)));
        }
        adapter.notifyDataSetChanged();
    }

    //Setup Player
    private void setController() {

        playButton.setOnClickListener(this);
        nextButton.setOnClickListener(this);
        prevButton.setOnClickListener(this);

        counterTextView.setText(String.valueOf(repeatCounter));

        //Increment and Show counter
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (repeatCounter == 0) stop();
                if (repeatCounter < tracks.size()-1) repeatCounter++;
                counterTextView.setText(String.valueOf(repeatCounter));
            }
        });

        //Decrement and Show counter
        subtractButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (repeatCounter > 1) repeatCounter--;
                counterTextView.setText(String.valueOf(repeatCounter));
            }
        });

        //Initialize and fill ArrayList
        tracks = new ArrayList<>();
        for (String s : descriptions) {
            tracks.add(new Audio(s, mDatabase.getAudioFile(s)));
        }
    }

    //Centralized onClick method for all Buttons
    @Override
    public void onClick(View v) {

        try {
            if (v.getId() == R.id.PlayButton) {

                //If repeat counter is 0 and user presses Play button, it will be inappropriate to
                // not to respond, instead what we do here is we explicitly set counter to 1
                // assuming that user wants to play at least once.
                if (repeatCounter == 0) {
                    repeatCounter = 1;
                    counterTextView.setText(Integer.toString(repeatCounter));
                }
                //Pause if playing and also set flag to true
                if (m != null && m.isPlaying()) {
                    m.pause();
                    isPaused = true;
                }
                //Resume if playing and set flag to false
                else if (isPaused) {
                    m.start();
                    isPaused = false;
                }
                //Otherwise if it is a fresh start
                else {
                    //Set current position to 0 i.e. first index in the ArrayList and Play
                    pos = 0;
                    String s = descriptions.get(pos);
                    titleTextView.setText(s);
                    play(mDatabase.getAudioFile(s), true);
                }
            } else if (v.getId() == R.id.NextButton) {
                //Play Next
                nextTrack();
            } else if (v.getId() == R.id.PrevButton) {
                //Play Previous
                prevTrack();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Play next track
    private void nextTrack() {
        //If below the last index
        if (pos < descriptions.size() - 1) {

            //Move one index ahead, fetch the info and Play
            pos++;
            String s = descriptions.get(pos);
            titleTextView.setText(s);
            outputFile = mDatabase.getAudioFile(s);

            try {
                play(outputFile, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //Play previous track
    private void prevTrack() {
        //If current position if above first index
        if (pos > 0) {

            //Move one index back, fetch the info and Play
            pos--;
            String s = descriptions.get(pos);
            titleTextView.setText(s);
            outputFile = mDatabase.getAudioFile(s);
            if (outputFile != null) {
                try {
                    play(outputFile, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                titleTextView.setText("No Audio with " + s);
            }
        }
    }


    //Because Playing an Audio in player requires only two thing i.e. Description and Audio File, I created a seperate class to hold these two.
    private class Audio {

        private String desc;
        private byte[] file;

        public Audio(String s, byte[] audioFile) {
            this.desc = s;
            this.file = audioFile;
        }

        public String getDesc() {
            return desc;
        }

        public byte[] getFile() {
            return file;
        }
    }
}