package com.exrebound.audiorecorder;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class NewRecordingActivity extends ActionBarActivity {

    @BindView(R.id.Title)
    TextView Title;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.EditTextDescription)
    EditText DescriptionEditText;
    @BindView(R.id.EditTextCategory)
    EditText CategoryEditText;
    @BindView(R.id.EditTextTag)
    EditText TagEditText;
    @BindView(R.id.Stars)
    RatingBar starsRatingBar;
    @BindView(R.id.ButtonRecord)
    Button RecordButton;
    @BindView(R.id.ButtonStop)
    Button StopButton;
    @BindView(R.id.ButtonSave)
    Button SaveButton;
    @BindView(R.id.container)
    LinearLayout container;

    //MediaRecorder instance to Record Audio
    private MediaRecorder myAudioRecorder;
    //File name to be saved
    private String fileName;

    //Database class instance
    RecordingsDatabase mDatabase;

    //Items to be taken via different Views
    String description;
    String category;
    String tag;
    String oldDescription;
    int stars;
    byte[] file;
    private Snackbar recordingSnackBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_new_recording);
        ButterKnife.bind(this);
        Title.setText("New Memo");
        setSupportActionBar(toolbar);

        /**
         * Get Category and Description of the Item from the intent.
         * Notice that Description String will be passed from RecordingsListActivity in case User opts
         * to Edit a particular Recording Item
         * */
        String tempDescription = getIntent().getStringExtra(RecordingsListActivity.EXTRA_DESCRIPTION);
        String tempCategory = getIntent().getStringExtra(CategoryListActivity.EXTRA_CATEGORY);

        //Little copy paste code from the Internet to turn the color of RatingBar star Yellow
        LayerDrawable d = (LayerDrawable) starsRatingBar.getProgressDrawable();
        d.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        //Make rating bar Increment 1 star every step. Default is 0.5 i.e. Half star
        starsRatingBar.setStepSize(1.0f);

        //Instantiate Database class
        mDatabase = new RecordingsDatabase(this);
        try {
            //Open DB
            mDatabase.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //In case of Edit, fetch all older values and place them in appropriate Views
        if ((tempDescription != null && tempDescription.length() > 0) && (tempCategory != null && tempCategory.length() > 0)) {
            oldDescription = tempDescription;
            category = tempCategory;
            DescriptionEditText.setText(oldDescription);
            TagEditText.setText(mDatabase.getTag(oldDescription));
            CategoryEditText.setText(category);
            CategoryEditText.setEnabled(true);
            RecordButton.setEnabled(true);
            starsRatingBar.setRating(mDatabase.getStars(oldDescription));
        }

        //Store number of Stars every time RatingsBar is changed
        starsRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                stars = (int) rating;
            }
        });

        //Keep onStopButtonClick button disabled until Record button is pressed
        StopButton.setEnabled(false);
    }

    @OnClick(R.id.ButtonRecord)
    protected void onRecordButtonClick() {
        //Basic AudioRecorder setup
        myAudioRecorder = new MediaRecorder();
        myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        fileName = NewRecordingActivity.this.getCacheDir().getAbsolutePath() + "/" + description + ".3gp";
        //Call the method to start recording and save it
        start();
    }

    @OnClick(R.id.ButtonSave)
    protected void onSaveButtonClick() {
        //If description and category fields are specified only then let the application save data
        if (DescriptionEditText.getText().length() > 0 && CategoryEditText.getText().length() > 0) {

            //Flag variable to indicate success of operation
            boolean success = true;
            try {
                //Fetch all info and save them in appropriate variable
                description = DescriptionEditText.getText().toString();
                tag = TagEditText.getText().toString();
                category = CategoryEditText.getText().toString();
                stars = (int) starsRatingBar.getRating();

                //If user is Editing item
                if (oldDescription != null) {
                    //Then fetch the Audio associated with it based on it's old description
                    file = mDatabase.getAudioFile(oldDescription);
                    //Also delete the old entry because it's of no use to us now
                    mDatabase.deleteEntry(oldDescription);
                }

                //Now while saving, if item has no recording
                if (fileName == null) {
                    //Insert entry with null value
                    mDatabase.insertEntry(description, category, tag, file, stars);
                } else {
                    //Otherwise use toByteArray method to convert the file into ByteArray and insert entry
                    mDatabase.insertEntry(description, category, tag, toByteArray(fileName), stars);
                }
            } catch (Exception e) {
                //If exception occurs, set flag to false
                success = false;
                //Create and display a dialog indicating user about error
                Utils.showAlertDialogWithoutCancel(NewRecordingActivity.this,
                        "Error", "Error While Saving To Database");
                e.printStackTrace();
            } finally {
                //Finally if operation finished successfully
                if (success) {
                    //Show dialog
                    Utils.showAlertDialog(NewRecordingActivity.this,
                            "Success", "File Successfully Saved To Database");
                }
            }
        } else {
            //In case Description or Category left empty, make toast to warn.
            Snackbar.make(container, "Please Provide Description and Category For Recording", Snackbar.LENGTH_SHORT).show();
        }
    }


    //Start recording
    protected void start() {
        try {
            //Set output file based on the given Filename, prepare and then start recording
            myAudioRecorder.setOutputFile(fileName);
            myAudioRecorder.prepare();
            myAudioRecorder.start();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Disable the Recording and Save button, and Enable Stop button
        RecordButton.setEnabled(false);
        StopButton.setEnabled(true);
        SaveButton.setEnabled(false);
        //Notify using Toast
        recordingSnackBar = Snackbar.make(container, "Recording...", Snackbar.LENGTH_INDEFINITE);
        recordingSnackBar.show();
    }


    //Stop recording and save
    @OnClick(R.id.ButtonStop)
    protected void onStopButtonClick() {
        myAudioRecorder.stop();
        myAudioRecorder.release();
        myAudioRecorder = null;
        StopButton.setEnabled(false);
        RecordButton.setEnabled(true);
        SaveButton.setEnabled(true);
        recordingSnackBar.dismiss();
        Snackbar.make(container, "Recording Completed!", Snackbar.LENGTH_SHORT).show();
    }


    //Convert file to byreArray
    private byte[] toByteArray(String FileName) {
        if (FileName != null && FileName.length() > 0) {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            BufferedInputStream in;
            try {
                in = new BufferedInputStream(new FileInputStream(FileName));
                int read;
                byte[] buff = new byte[1024];
                while ((read = in.read(buff)) > 0) {
                    out.write(buff, 0, read);
                }
                out.flush();
                return out.toByteArray();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
