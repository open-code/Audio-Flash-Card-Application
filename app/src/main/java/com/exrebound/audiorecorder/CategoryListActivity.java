package com.exrebound.audiorecorder;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

/*First Activity to Launch at the startup*/
public class CategoryListActivity extends ActionBarActivity {
    //Identifier Tag that will go along with Intent Extra
    public static final String EXTRA_CATEGORY = ".CATEGORY";

    //Array List to hold categories
    ArrayList<String> categories;
    //Adapter to manipulate categories ArrayList
    ArrayAdapter<String> adapter;

    //Database instance
    RecordingsDatabase mDatabase;

    @BindView(R.id.Title)
    TextView Title;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.categoryList)
    ListView categoriesList;
    @BindView(R.id.FLNoTeams)
    FrameLayout FLNoMemos;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);
        ButterKnife.bind(this);
        Title.setText("Categories");
        setSupportActionBar(toolbar);

        //Instantiate database
        mDatabase = new RecordingsDatabase(this);
        try {
            //Open Database and call setListView method that will extract data from DB and set it up in ListView
            mDatabase.open();
            setListView();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Helper method to create and setup ListView
    private void setListView() {
        categoriesList.setEmptyView(FLNoMemos);
        //Call getCategories method from our Database class to provide ArrayList of categories in the Database
        categories = mDatabase.getCategories();
        //Use adapter to wire Categories ArrayList with ListView
        adapter = new ArrayAdapter<>(this, R.layout.category_list_item, categories);
        categoriesList.setAdapter(adapter);
        //Register the ListView to be able to respond to Context Menu events
        registerForContextMenu(categoriesList);
    }

    @OnItemClick(R.id.categoryList)
    protected void onCategoriesListItemClick(int position) {
        //Get String from ListView
        String category = (String) categoriesList.getItemAtPosition(position);

        //Create new Intent and pass it the String we fetched along with Identifier Tag that we defined in the beginning
        Intent i = new Intent(CategoryListActivity.this, RecordingsListActivity.class);
        i.putExtra(EXTRA_CATEGORY, category);
        //Start new activity
        startActivity(i);
    }

    @OnClick(R.id.fab)
    protected void onFABClick() {
        //Start NewRecordingActivity
        startActivityForResult(new Intent(this, NewRecordingActivity.class), 0);
    }

    //Refresh List and ListViews every time application is Paused or Resumed
    @Override
    protected void onResume() {
        super.onResume();
        //Remove all iems from ArrayList
        categories.clear();
        //Again call getCategories method to fetch fresh set of items from Database
        categories.addAll(mDatabase.getCategories());
        //Call method to Notify the Adapter about change in ArrayList that it is manipulating
        adapter.notifyDataSetChanged();
    }

    //Whenever a List item is long clicked, Inflate Context Menu from given Layout Resource
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(
                R.menu.categories_list_context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // Extract information about selected item
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int position = info.position;
        //Extract Name of Category from the given position
        String category = categories.get(position).toString();

        //Check for which option is selected in the Context Menu
        switch (item.getItemId()) {
            //If ID matches Delete Category button
            case R.id.MIDeleteCategory:
                //Call Database's method to delete entire Category using it's name
                mDatabase.deleteAll(category);
                //Refreshing phase
                categories.clear();
                categories.addAll(mDatabase.getCategories());
                adapter.notifyDataSetChanged();
                return true;
            //If ID matches Delete All button
            case R.id.MIDeleteAll:
                //Call DeleteAll method to remove all Categories from the Database
                mDatabase.deleteAll();
                //Refresh
                categories.clear();
                categories.addAll(mDatabase.getCategories());
                adapter.notifyDataSetChanged();
                return true;
        }
        return super.onContextItemSelected(item);
    }

    //Close Database when application is completely closed
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDatabase.close();
    }

    //When focus comes back from NewRecordingActivity to this Activity, Refresh ArrayList to update ListView
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        categories.clear();
        categories.addAll(mDatabase.getCategories());
        adapter.notifyDataSetChanged();
    }
}